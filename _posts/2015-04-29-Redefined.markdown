---
title:  "Redefined"
date:   2015-04-29 09:45:36
image: camp-sass.jpg
---

This one is close to my heart guys…… I hope it gives you the strength/ practical tips to help you redefine yourself.

On my way to work I saw the poster below…..

What really cut my attention was the words …….

under this gloves is a beautiful manicure

It made me laugh , to me it was like her way of telling the world to shut up and shove all their stereotypes of what they expected her to be. She was going to be the only one that got to define herself.  I was thinking go girl you tell them… and while you are at it bunch anyone that tries to stop you…LOL !!!!!!

We live in a world where people are very quick to judge you, they expect you to fit into one box as this makes them more comfortable. As human beings we like certainty and anything outside that certainty makes us uncomfortable. So subconsciously  we try to conform to just being one thing as this will fulfill the  pre-constructed stereotypes that society has made,not because we like it but it is what we have been taught is “normal”… (that is what we tell ourselves ), when truly it has just been us being too afraid to step out of our comfort zone.

I used to try to decide  on the one thing I wanted to be. But for some reason I could never settle on one thing… it always felt like I was missing something. (now i know why LOL.)  Until one day it finally hit me why in Gods name would I ever want to be one thing when I can be so much more.??? Why could I not be everything I wanted to be ??

When I look back at those times I tried to decide on one career path… I laugh at myself… I imagine being a child in this amazing ice-cream shop…. I mean for real guys this ice-cream shop has all the flavors of ice-cream I could have ever dreamed of and more .. then my mum comes in and tells me that I can only pick one flavor of ice-cream…. I am thinking mother why have you done this to me …But I want so much more!!!!!

It is the same thing with choosing a career path…..why do you have to just be a lawyer… why can’t you be a lawyer and chef….. I am not saying in any way that choosing one career path is wrong… I am just saying it is ok… to chose more than one path.

I know you are probably thinking … Pls don’t even get me started on all the paths  I want to take…. it is probably impossible to do all of them at once there is just one of me !!!!!!!

Yes yes I know there is only one of you… so below you will find 5 practical tips to get you started…. All you will need is a pen & paper.

1) Take a pen and a paper and write a list of all career paths you have ever wanted to follow and could never decide on………….Yes even the stupid ones.

2) Narrow them down to the top 5 (within reason )

3) Decided on the first one you think you can begin to work on.

4) You are going to need to focus on that one for how ever long it takes you to get that up and running……

5) Move on to number 2 on that list…. then repeat step 4.

This is probably easier said than done… P.S this will take some time ….so this is not for the faint hearted ….  you have been warned  

As always pls like, share and leave all your comments below.

Like my page on Facebook  – https://www.facebook.com/EverydaypeoplebyLydia
Email me – everydaypeoplebylydia@yahoo.com
Instagram- everydaypeoplebylydia

Mirror Mirror on the wall…………
JUNE 23, 2015	~ 2 COMMENTS
One of the most powerful quotes I have ever read…. That truly changed me was

The best way to predict the future is to create it

It is a very simple quote… But it tells a big story….. it tells the story of the constant battle we all fight with ourselves.

It took me a really long time to fully understand what this quote was telling me, until one day it finally clicked.I can only explain it in one word self-discipline. The dictionary defines self-discipline as the ability to control one’s feelings and overcome one’s weaknesses.

I finally understood this quote when I looked in the mirror…. And the mirror did not reflect what I expected to see. I did not see a person that was even remotely close to who I truly was on the inside. I finally realized that, that person only existed in my head

The big question is how do you pull the person in your head out so that one day when you look in the mirror, your reflection is actually who you see yourself as.

It all comes down to self-discipline….. for only after we have conquered the enemy inside can we even begin to fight the enemy outside.

I personally had to learn this the hard way… because of the course I study.  I have to go on placements. My placements start at 8:00 AM that meant I had to be up everyday at 6:00 AM, this also meant I had to be in bed for 10:00 PM to allow me get my much need 8 hrs of sleep. For a night owl like me it was a big struggle, but every time forced my self to go sleep at 10:00 PM. I was grateful the next morning.But when I did not get to bed for 10:00 PM I paid for it dearly the next day.

I can promise you that it will not be easy, I promise that it will suck most of the time,but I also promise you that  at the end of the day you will be grateful for every min of it.

So I Challenge you to take a good look at the mirror and if you are not happy with your reflection… try a little self-discipline….. I promise you will be glad you did.


